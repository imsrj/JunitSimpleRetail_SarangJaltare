package com.retail.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestAirCalc {
	Item item1, item2;
	
	@Before
	public void setUp() {
		
		GetItemFactory getItemFactory = new GetItemFactory();
  	    item1 = getItemFactory.getItem("AIR");
  	    item2 = getItemFactory.getItem("GROUND");
  	    
		
	}
	    
	@Test
	public void testAirShippingCostPositive() {
		   
		  item1.setUpc(567321101987l);
    	  item1.setDescription("CD - PinkFloyd, Dark Side of the Moon");
    	  item1.setPrice(19.99);
    	  item1.setWeight(5);
    	  assertEquals(40, item1.getShippingCost());
	}
	
	@Test(expected = IllegalArgumentException.class)
	  public void testAirShippingCostNegative() {
		
		 item1.setUpc(123l);
   	     item1.setDescription("CD - PinkFloyd, Dark Side of the Moon");
   	     item1.setPrice(19.99);
   	     item1.setWeight(-5);
	   
	    
	  }
	
	@Test(expected = NullPointerException.class)
	  public void testAirShippingCostNull() {
		
		 item1.setUpc(123l);
 	     item1.setDescription(null);
 	     item1.setPrice(-19.99);
 	     item1.setWeight(5);
	   
	}
	

	@Test
	public void testGroundShippingCostPositive() {
		   
	  item2.setUpc(567321101987l);
  	  item2.setDescription("CD - PinkFloyd, Dark Side of the Moon");
  	  item2.setPrice(19.99);
  	  item2.setWeight(5);
  	  assertEquals(12.5, item2.getShippingCost(), 0.0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGroundShippingCostNegative() {
		   
	  item2.setUpc(567321101987l);
  	  item2.setDescription("CD - PinkFloyd, Dark Side of the Moon");
  	  item2.setPrice(19.99);
  	  item2.setWeight(-5);
  	 
	}
	
	
	
	

}
